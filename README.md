# Arranged or Transposed Music in LilyPond
Here are presented sources for LilyPond compiler. Usually these sources are based on sheets or MIDI music flying around the Internet.

The main reason why I'm doing such work is the absence of sheets for Alto Sax (or I've just failed getting one).

## Content list
- He's a pirate (cut down to single voice & transposed to be in the playable range);
- Imperial march (based on midi file, has some inaccurate data).

