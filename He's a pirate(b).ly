\version "2.22.0"

\header{
    title = "He's A Pirate"
    subtitle = "Originaly arranged by NickZammit15 at musescore.com"
    composer = "Transposed by Fe-Ti"
    instrument = "Alto Sax"
}

\score {
    \new Staff
        \with {midiInstrument = "Alto Sax"}
        \relative c' {
            \tempo "Briskly" 4 = 210
            \key e \minor
            \time 6/8
            r2 b8 d
            e4 e e8 fis
            g4 g g8 a
            fis4 fis e8 d
            d e4
            
            r8 b d
            e4 e e8 fis
            g4 g g8 a
            fis4 fis e8 d e4
            
            r b8 d
            e4 e e8 g
            a4 a a8 b
            c4 c b8 a
            g e4 
            
            r8 e8 fis g4 g a g8 e4
            
            r8 e g
            fis4 fis g8 e
            fis4
            
            r b8 d
            e4 e e8 fis
            g4 g g8 a
            fis4 fis e8 d
            d e4
            
            r8 b d
            e4 e e8 fis
            g4 g g8 a
            fis4 fis e8 d e4
            r b8 d
            e4 e e8 g
            a4 a a8 b
            c4 c b8 a
            b e,4
            
            r8 e fis g4 g a b8 e,4
            
            r8 e g fis4 fis e8 d
            e4 e fis g g8 g a4
            
            b2 g8 e b2 r4
            c'2 a8 e c2 r4
            
            e,4. g fis
            
            r8 g a b4 b b c8 b4
            
            r8 r4 a a a a8 b4
            r8 r4 b b b c8 b4
            r8 r4 a g fis e
            
            r e8 fis g2 a8 b
            a4 g fis g a b a
            r g8 a b4
            r a8 g fis4 g fis e
            r fis8 d e4
            r e'8 fis g4
            r fis8 g a4 g a
            b a g e
            r e8 fis g4 a b c e, a g
            r a8 fis e4
            r fis8 dis b'4 r2 c4 r2
            b4 b b b8 r a4 r a r2 g4
            
            r2 g4 a fis e8
            r4 fis8 g a b
            r4 e,8 fis g c
            r4 e,8 fis g b4 b d b8 a4
            r4. a4 r2 g4 r2 g4 a fis e4.
            r8 r4 e,2. \bar "|."
        }
    \layout{}
    \midi{}
}
