\version "2.22.0"
% WIP
\header{
    title = 	"Merry-Go-Round Of Life"
    composer = 	"Composed by Joe Hisaishi"
    arranger = 	"Originaly arranged by Alex Patience"
    opus = 	"Simplified by Fe-Ti from piano score"
    instrument = "Alto Sax (WIP)"
}

\score {
    \new Staff
        \with {midiInstrument = "Alto Sax"}
        \relative c'' {
            \tempo 4 = 100
            \key bes \major
            \time 3/4
            d8[es] d[es]  d[es]
            c [d] c[d] c[d]
            bes[c] bes[c] bes[c]
            a16 g fis2 r8
            r4 r d( d2.)
            d4 g bes d2 d4 c( bes a bes2.)
            r4. g8 bes d g2 g4 g( f es f2.)
            r4. a,8 d f a2 g4 f( e f g2) f4 e2( d4) c( bes c) d c g a
            r8 d, g a \tuplet 5/4 {  es8. a c bes4. } a4( fis2)
            r4 c'8 bes4. a8 g8. fis8. es4 d2.(d2.)
            r2. r r 
            
        }
    \layout{}
    \midi{}
}
