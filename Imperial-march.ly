\version "2.14.0"

\header{
    title = "Imperial march"
    composer = "Arranged by Fe-Ti"
    instrument = "Alto Sax"
}

\score {
    \new Staff
        \with {midiInstrument = "alto sax"}
        \relative c'' {
            \time 4/4
            \tempo 4=120
            a4 a a f8. c'16 a4 f8. c'16 a4 r
            e'4 e e f8. c16 gis4 f8. c'16 a4 r
            
            a'4 a,8. a16 a'4 gis8 g fis16( f fis) r bes,8 es4 r8
            d8 cis c16( b c) r f,8 gis4 r8 f8. a16 c4 a8. c16 e4 r
            
            a4 a,8. a16 a'4 gis8 g fis16( f fis) r bes,8 es4 r8
            d8 cis c16( b c) r f,8 gis4 r8 f8. c'16 a4 f8. c'16 a4 r
            
        }
        \layout {}
        \midi {}
}